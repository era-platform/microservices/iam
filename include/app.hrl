%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.05.2021

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(ClassesCN, <<"classes">>).
-define(RolesCN, <<"roles">>).
-define(UsersCN, <<"users">>).

-define(MasterStorageInstance, <<"master">>).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(PLATFORMLIB, platformlib).

-define(MsvcDms, <<"dms">>).

-define(APP, iam).
-define(MsvcType, <<"iam">>).

-define(SUPV, iam_supv).
-define(CFG, iam_config).

-define(FixturerCallback, iam_fixturer_callback).
-define(FixturerLeaderName, 'iam_fixturer_leader_srv').

-define(FixturerClasses, iam_fixture_coll_classes).
-define(FixturerRoles, iam_fixture_coll_roles).
-define(FixturerUsers, iam_fixture_coll_users).

-define(VALIDATOR, iam_validator_srv).
-define(ValidatorRoles, iam_validator_coll_roles).
-define(ValidatorUsers, iam_validator_coll_users).

-define(DomainTreeCallback, iam_domaintree_callback).

-define(DSUPV, iam_domain_supv).
-define(DSRV, iam_domain_srv).
-define(DEndpoints, iam_domain_endpoints).
-define(DSearchSrv, iam_domain_search_srv).
-define(DModifySrv, iam_domain_modify_srv).
-define(DSearchUtils, iam_domain_search).
-define(DU, iam_domain_utils).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).

%% ------
%% From platformlib
%% ------
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(LeaderSupv, platformlib_leader_app_supv).
-define(LeaderU, platformlib_leader_app_utils).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CRUD, platformlib_dms_crud).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_FIXTURER_SUPV, platformlib_dms_fixturer_leader_supv).

-define(DTREE_SUPV, platformlib_domaintree_supv).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {iam,iam}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================
