%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Validates 'session' entity on any modify operation.

-module(iam_validator_coll_users).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=_EntityInfo,_ModifierInfo) ->
    FunIsFixt = fun(_E) -> false end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        _ when Operation=='delete' ->
            {ok,undefined};
        _ ->
            case check_entity(Domain,Operation,{E0,E1}) of
                {error,_}=Err -> Err;
                {ok,EntityV} ->
                    case validate_all(Domain,Operation,{E0,EntityV}) of
                        ok -> {ok,EntityV};
                        {error,_}=Err -> Err
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check properties
%% TODO
%% -------------------------------------
check_entity(Domain,Operation,{Item0,Item}) ->
    check_1(Domain,Operation,{Item0,Item}).

%% password
check_1(Domain,Operation,{Item0,#{<<"password">>:=Pwd}=Item}) when is_binary(Pwd) ->
    case re:run(Pwd,<<"^\\*+$">>) of
        nomatch -> check_1b(Domain,Operation,Item);
        _ when Operation=='update' -> check_1b(Domain,Operation,Item#{<<"password">> := maps:get(<<"password">>,Item0)});
        _ -> check_1b(Domain,Operation,Item#{<<"password">> := maps:get(<<"password">>,Item0)})
    end;
check_1(_,_,_) ->
    {error, {invalid_params, <<"field=password|Invalid password. Must be string">>}}.
%% dehash pwd
check_1b(Domain,Operation,Item) ->
    % TODO: dehash password
    check_2(Domain,Operation,Item).

%% name, login, password
check_2(Domain,Operation,Item) ->
    Name = maps:get(<<"name">>,Item),
    Login = maps:get(<<"login">>,Item),
    Pwd = maps:get(<<"password">>,Item),
    case {length(?BU:to_list(Name)), length(?BU:to_list(Login)), length(?BU:to_list(Pwd))} of
        {_N, _L, _P} when _N>1000 ->
            {error, {invalid_params, <<"field=name|name must be less than 1000 symbols">>}};
        {_N, _L, _P} when _L>100 ->
            {error, {invalid_params, <<"field=login|login must be less than 100 symbols">>}};
        {_N, _L, _P} when _P>100 ->
            {error, {invalid_params, <<"field=password|password must be less than 100 symbols">>}};
        {_N, _L, _P} ->
            Flogin = fun(A) when (A>=$A andalso A=<$Z)
                            orelse (A>=$a andalso A=<$z)
                            orelse (A>=$0 andalso A=<$9)
                            orelse (A==$_) orelse (A==$-) orelse (A==$.) orelse (A==$~) orelse (A==$!) -> true;
                        (_) -> false
                     end,
            Fpwd = fun(A) when (A>=$A andalso A=<$Z)
                          orelse (A>=$a andalso A=<$z)
                          orelse (A>=$0 andalso A=<$9)
                          orelse (A==$_) orelse (A==$-) orelse (A==$.) orelse (A==$~) orelse (A==$!)
                          orelse (A==$@) orelse (A==$#) orelse (A==$$) orelse (A==$%) -> true;
                      (_) -> false
                    end,
            case {lists:all(Flogin, ?BU:to_list(Login)), lists:all(Fpwd, ?BU:to_list(Pwd))} of
                {false, _} ->
                    {error, {invalid_params, <<"field=login|login contains invalid symbols. Expected [A-Za-z0-9_-.~!]">>}};
                {_, false} ->
                    {error, {invalid_params, <<"field=password|password contains invalid symbols. Expected [A-Za-z0-9_-.~!@#$%]">>}};
                {true, true} ->
                    check_3(Domain,Operation,Item)
            end end.

%% roles
check_3(Domain,Operation,Item) ->
    Roles = maps:get(<<"roles">>,Item),
    case ?DMS_CACHE:read_cache(Domain,?RolesCN,#{},auto) of
        {ok,RoleItems,_} ->
            Known = maps:from_list(lists:map(fun(RoleItem) -> {maps:get(<<"id">>,RoleItem),true} end, RoleItems)),
            case lists:partition(fun(Lnk) -> maps:is_key(Lnk,Known) end, Roles) of
                {_,[]} -> check_x(Domain,Operation,Item);
                {_,Unknown} -> {error,{internal_error,?BU:strbin("field=roles|Invalid roles. Found unknown roles: ~120tp",[?BU:join_binary_quoted(Unknown,<<"'">>,<<" ,">>)])}}
            end;
        T ->
            ?LOG('$error',"Validator read roles: ~120tp",[T]),
            {error,{internal_error,<<"Validator cannot read roles">>}}
    end.

%% final
check_x(_Domain,_Operation,Item) -> {ok,Item}.

%% -------------------------------------
%% Check all users in complex
%% -------------------------------------
validate_all(_Domain,'delete',_) -> ok;
validate_all(Domain,_Operation,{_E0,Item}) ->
    Id = maps:get(<<"id">>,Item),
    Login = maps:get(<<"login">>,Item),
    AllUsers = case ?DMS_CACHE:read_cache(Domain,?UsersCN,#{},auto) of
                   {ok,UserItems,_} -> lists:filter(fun(UserItem) -> maps:get(<<"id">>,UserItem) /= Id end, UserItems);
                   T ->
                       ?LOG('$error',"Validator read users: ~120tp",[T]),
                       {error,{internal_error,<<"Validator cannot read users">>}}
               end,
    case lists:filter(fun(UserItem) -> maps:get(<<"login">>,UserItem) == Login end, AllUsers) of
        [_|_] -> {error, {invalid_params, <<"field=login|login already exists">>}};
        [] -> ok
    end.
