%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Validates 'session' entity on any modify operation.

-module(iam_validator_coll_roles).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=_EntityInfo,_ModifierInfo) ->
    FunIsFixt = fun(_E) -> false end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        _ when Operation=='delete' ->
            case validate_all(Domain,Operation,{E0,E0}) of
                ok -> {ok,undefined};
                {error,_}=Err -> Err
            end;
        _ ->
            case check_entity(Domain,Operation,E1) of
                {error,_}=Err -> Err;
                {ok,EntityV} ->
                    case validate_all(Domain,Operation,{E0,EntityV}) of
                        ok -> {ok,EntityV};
                        {error,_}=Err -> Err
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check properties
%% -------------------------------------
check_entity(Domain,Operation,Item) ->
    check_1(Domain,Operation,Item).

%% code
check_1(Domain,Operation,Item) ->
    case maps:get(<<"code">>,Item) of
        <<>> -> {error, {invalid_params, <<"field=code|Invalid 'code'. Must not be empty">>}};
        Name when is_binary(Name) ->
            F = fun($/) -> true;
                   ($#) -> true;
                   ($@) -> true;
                   ($() -> true;
                   ($)) -> true;
                   ($;) -> true;
                   ($,) -> true;
                   ($.) -> true;
                   ($:) -> true;
                   ($_) -> true;
                   ($-) -> true;
                   ($~) -> true;
                   ($!) -> true;
                   (A) when (A>=$a andalso A=<$z) -> true;
                   (A) when (A>=$A andalso A=<$Z) -> true;
                   (A) when (A>=$0 andalso A=<$9) -> true;
                   (_) -> false
                end,
            case lists:all(F, ?BU:to_list(Name)) of
                true -> check_2(Domain,Operation,Item);
                false -> {error, {invalid_params, <<"field=code|Invalid code. Contains invalid symbols. Expected: [A-Za-z0-9/#@();,.:_-~!]">>}}
            end
    end.

%% parent_id
check_2(Domain,Operation,Item) ->
    EmptyId = ?BU:emptyid(),
    case maps:get(<<"parent_id">>,Item,undefined) of
        P when P==undefined; P==<<>>; P==EmptyId -> check_3(Domain,Operation,maps:without([<<"parent_id">>],Item));
        IdClass when is_binary(IdClass) -> check_3(Domain,Operation,Item)
    end.

%% restrict_endpoints
check_3(Domain,Operation,Item) ->
    K = <<"restrict_endpoints">>,
    Endpoints = maps:get(K,Item,[]),
    case check_endpoints(K,Endpoints) of
        {ok,EPs1} -> check_4(Domain,Operation,Item#{K => EPs1});
        {error,_}=Err -> Err
    end.

%% permit_endpoints
check_4(Domain,Operation,Item) ->
    K = <<"permit_endpoints">>,
    Endpoints = maps:get(K,Item,[]),
    case check_endpoints(K,Endpoints) of
        {ok,EPs1} -> check_x(Domain,Operation,Item#{K => EPs1});
        {error,_}=Err -> Err
    end.

%% final
check_x(_Domain,_Operation,Item) -> {ok,Item}.

%% ---------------------------------------------------------------
%% check endpoints
%% ---------------------------------------------------------------

%% Samples
%%  { "url": "/rest/v1/iam/sessions/current", "methods": ["*"] },
%%  { "url": "/rest/v1/iam/users/current", "methods": ["GET","OPTIONS"] },
%%  { "url": "/rest/v1/metadata/**", "methods": ["GET","OPTIONS"] },
%%  { "url": "/rest/v1/addressbook/*/*", "methods": ["GET"] },
%%  { "url": "/ws#scriptnotify", "methods": ["WEBSOCKET"] },

check_endpoints(K,Endpoints) ->
    Res = lists:foldl(
        fun(_,{error,_}=Err) -> Err;
           (EP,_) when not is_map(EP) -> {error,{invalid_params, ?BU:strbin("field=~ts|Invalid ~ts. List should contain objects",[K,K])}};
           (EP,Acc) ->
              Res = case {maps:get(<<"url">>,EP,undefined), maps:get(<<"methods">>,EP,undefined)} of
                        {<<"/",_/binary>>=X,Y} when is_list(Y) -> {ok,X,Y};
                        {X,Y} when X==undefined; Y==undefined ->
                            {error,{invalid_params, ?BU:strbin("field=~ts|Invalid ~ts. Route object should contain endpoint declaration keys: 'url' and 'methods'",[K,K])}};
                        {X,_} when not is_binary(X) ->
                            {error,{invalid_params, ?BU:strbin("field=~ts|Invalid ~ts. Endpoint's 'url' should be a string",[K,K])}};
                        {_,Y} when not is_list(Y) ->
                            {error,{invalid_params, ?BU:strbin("field=~ts|Invalid ~ts. Endpoint's 'methods' should be a list of strings (any HTTP method, 'WEBSOCK' or '*')",[K,K])}};
                        {_,_} ->
                            {error,{invalid_params, ?BU:strbin("field=~ts|Invalid ~ts. Endpoint's 'url' should start with '/'",[K,K])}}
                    end,
              case Res of
                  {ok,Url,Methods} -> [#{<<"url">> => Url,
                                         <<"methods">> => Methods}
                                       |Acc];
                  {error,_}=Err -> Err
              end end, [], Endpoints),
    case Res of
        {error,_}=Err -> Err;
        _ when is_list(Res) -> {ok,Res}
    end.

%% -------------------------------------
%% Check all roles in complex
%% -------------------------------------
validate_all(Domain,Operation,{_E0,Item}) ->
    Id = maps:get(<<"id">>,Item),
    Code = maps:get(<<"code">>,Item),
    AllRoles = case ?DMS_CACHE:read_cache(Domain,?RolesCN,#{},auto) of
                   {ok,RoleItems,_} -> lists:filter(fun(RoleItem) -> maps:get(<<"id">>,RoleItem) /= Id end, RoleItems);
                   T ->
                       ?LOG('$error',"Validator read roles: ~120tp",[T]),
                       {error,{internal_error,<<"Validator cannot read roles">>}}
               end,
    case Operation of
        'delete' -> check_nesting_tree('down',Item,AllRoles);
        O when O=='create';O=='replace';O=='update';O=='delete' ->
            case lists:filter(fun(RoleItem) -> maps:get(<<"classname">>,RoleItem) == Code end, AllRoles) of
                [_|_] -> {error, {invalid_params, <<"field=code|code already exists">>}};
                [] -> check_nesting_tree('up',Item,AllRoles)
            end end.

%% -------------------------------------
%% Check nesting tree. If parent exists, if nesting tree is not cycled
%% -------------------------------------
check_nesting_tree(up,Item,AllRoles) ->
    Id = maps:get(<<"id">>,Item),
    EmptyId = ?BU:emptyid(),
    F = fun F(ItemX,Level) ->
                Find = fun(IdRoleX) -> lists:filter(fun(RoleItem) -> maps:get(<<"id">>,RoleItem) == IdRoleX end, AllRoles) end,
                case maps:get(<<"parent_id">>,ItemX,undefined) of
                    undefined -> ok;
                    EmptyId -> ok;
                    IdRole ->
                        case Find(IdRole) of
                            [] when Level==0 ->
                                {error, {invalid_params, <<"field=parent_id|Invalid parent_id. Expected id of existing role or empty">>}};
                            [] when Level>0 ->
                                {error, {invalid_params, <<"field=parent_id|Invalid parent_id. Nesting tree is broken">>}};
                            [#{<<"id">> := ParentId}] when ParentId==Id ->
                                {error, {invalid_params, <<"field=parent_id|Invalid parent_id. Nesting cycle detected">>}};
                            [ParentItem] -> F(ParentItem,Level+1)
                        end end end,
    F(Item,0);
%%
check_nesting_tree(down,Item,AllRoles) ->
    Id = maps:get(id,Item),
    F = fun(V, {N,L}=Acc) ->
                K = maps:get(<<"code">>,V),
                % check nested classes
                case maps:get(<<"parent_id">>,V,undefined) of
                    ParentId when ParentId==Id -> {[K|N],L};
                    _ ->
                        % check classes linked by properties
                        F = fun(Prop) ->
                                case maps:get(<<"data_type">>,Prop) of
                                    <<"entity">> -> maps:get(<<"idclass">>,Prop) == Id;
                                    _ -> false
                                end end,
                        case lists:any(F,maps:get(<<"properties">>,V)) of
                            true -> {N,[K|L]};
                            false -> Acc
                        end end end,
    case lists:foldl(F,{[],[]},AllRoles) of
        {[],[]} -> ok;
        {Nests,[]} -> {error,{invalid_operation,?BU:strbin("Role is nested by ~ts", [?BU:join_binary_quoted(Nests,<<"'">>,<<",">>)])}};
        {[],Links} -> {error,{invalid_operation,?BU:strbin("Role is linked by ~ts", [?BU:join_binary_quoted(Links,<<"'">>,<<",">>)])}};
        {Nests,Links} ->
            {error,{invalid_operation,?BU:strbin("Role is nested by ~ts; linked by ~ts", [?BU:join_binary_quoted(Nests,<<"'">>,<<",">>),?BU:join_binary_quoted(Links,<<"'">>,<<",">>)])}}
    end.