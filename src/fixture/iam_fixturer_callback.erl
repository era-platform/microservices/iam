%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Callbacks for fixturer leader

-module(iam_fixturer_callback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([app/0,
         get_nodes/0,
         log/2,
         fixtures/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Callback functions
%% ====================================================================

app() ->
    ?APP.

get_nodes() ->
    ?PCFG:get_nodes_by_msvc(?MsvcType).

log(Fmt,Args) ->
    ?LOG('$info',Fmt,Args).

fixtures(Domain) ->
    [{?ClassesCN, [?FixturerClasses:roles(Domain),?FixturerClasses:users(Domain)]},
     {?RolesCN, [?FixturerRoles:admin(Domain)],
        fun(Item) -> maps:get(<<"id">>,Item)==<<"4c9c83cb-017a-42a3-612f-7cd30a921f58">> end},
     {?UsersCN, [?FixturerUsers:admin(Domain)],
        fun(Item) -> case maps:get(<<"id">>,Item) of
                         <<"40b1ff1d-017a-46fe-3378-7cd30a921f58">> -> {true, maps:with([<<"id">>,<<"login">>,<<"name">>,<<"roles">>],Item)};
                         _ -> false
                     end end,
        fun(Item) -> maps:with([<<"id">>,<<"login">>,<<"name">>,<<"roles">>],Item) end}].

%% ====================================================================
%% Internal functions
%% ====================================================================