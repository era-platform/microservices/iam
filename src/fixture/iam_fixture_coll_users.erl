%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.06.2021
%%% @doc

-module(iam_fixture_coll_users).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([admin/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================


%% -------------------------------------
%% ROLE "admin"
%% return fixtured entity of role
%% -------------------------------------
admin(_Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"40b1ff1d-017a-46fe-3378-7cd30a921f58">>),
        <<"name">> => <<"Administrator">>,
        <<"login">> => <<"admin">>,
        <<"password">> => <<"admin">>,
        <<"roles">> => [<<"4c9c83cb-017a-42a3-612f-7cd30a921f58">>]
    }.

%% ====================================================================
%% Internal functions
%% ====================================================================