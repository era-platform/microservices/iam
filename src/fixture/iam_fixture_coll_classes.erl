%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Controlled classes descriptions (for dms collection 'classes')

-module(iam_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([roles/1,
         users/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "roles"
%% return fixtured entity of controlled class
%% -------------------------------------
roles(Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"bb558158-017a-4296-5c49-7cd30a921f58">>),
        <<"classname">> => ?RolesCN,
        <<"name">> => <<"roles">>,
        <<"description">> => <<"General platform collection. Roles of users.">>,
        <<"storage_mode">> => <<"category">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => <<"iam:validator">>,
            <<"appserver">> => ?GN_NAMING:get_globalname(?MsvcType,Domain),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => true,
            <<"notify_integrity_timeout">> => 5000,
            <<"max_limit">> => 1000,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"partition_property">> => <<>>,
            <<"lookup_properties">> => [<<"code">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"code">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"title">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"description">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"parent_id">>,
                <<"data_type">> => <<"entity">>,
                <<"idclass">> => <<"bb558158-017a-4296-5c49-7cd30a921f58">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"restrict_endpoints">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => true
            },
            #{
                <<"name">> => <<"permit_endpoints">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => true
            }
        ]}.

%% -------------------------------------
%% CLASS "users"
%% return fixtured entity of controlled class
%% -------------------------------------
users(Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"36ae9ba0-017a-4296-5e79-7cd30a921f58">>),
        <<"classname">> => ?UsersCN,
        <<"name">> => <<"users">>,
        <<"description">> => <<"General platform collection. Users.">>,
        <<"storage_mode">> => <<"category">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => <<"iam:validator">>,
            <<"appserver">> => ?GN_NAMING:get_globalname(?MsvcType,Domain),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false,
            <<"notify_integrity_timeout">> => 5000,
            <<"max_limit">> => 1000,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"partition_property">> => <<>>,
            <<"lookup_properties">> => [<<"login">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"name">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"login">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"password">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false,
                <<"hidden">> => true, % TODO
                <<"hidden_value">> => <<"***">> % TODO
            },
            #{
                <<"name">> => <<"roles">>,
                <<"data_type">> => <<"entity">>,
                <<"idclass">> => <<"bb558158-017a-4296-5c49-7cd30a921f58">>,
                <<"required">> => false,
                <<"default">> => [],
                <<"multi">> => true
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================