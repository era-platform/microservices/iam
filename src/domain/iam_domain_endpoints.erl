%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.06.2021
%%% @doc

-module(iam_domain_endpoints).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([authorize/4]).
-export([build_endpoint_rules/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(endpoint_rule, {
    access_mode :: public | authenticated | role,
    role_id :: undefined | binary(),
    url :: binary(),
    methods :: [binary()]
}).

-define(Permit,'permit').
-define(Restrict,'restrict').

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% performs authorize operation to endpoint
%% ------------------------------------------
-spec authorize(Roles::[RoleId::binary()], Endpoint::binary(), Method::binary(), EtsHash::ets:tab())
      -> ok | {error,{access_denied,Reason::binary()}}.
%% ------------------------------------------
authorize(Roles,Endpoint,Method,EtsHash) when is_list(Roles), is_binary(Endpoint), is_binary(Method) ->
    Candidates = candidates(binary:split(Endpoint,<<"/">>,[global,trim_all])),
    PermitCandidates = lists:map(fun(C) -> {?Permit,C} end, Candidates),
    RestrictCandidates = lists:reverse(lists:map(fun(C) -> {?Restrict,C} end, Candidates)),
    case {is_matched(Roles,RestrictCandidates,Method,EtsHash), is_matched(Roles,PermitCandidates,Method,EtsHash)} of
        {true,_} -> {error,{access_denied,<<"Access restricted">>}};
        {false,false} -> {error,{access_denied,<<"Access denied">>}};
        {false,true} -> ok
    end.

%% ------------------------------------------
%% Build hash of endpoints
%%   {Mode :: permit | restrict, Parts :: [Part :: binary()]} ====> [Rule :: #endpoint_rule{}]
%% ------------------------------------------
-spec build_endpoint_rules(RoleItems::[map()], Domain::binary() | atom(), EtsHash::ets:tab()) -> ok.
%% ------------------------------------------
build_endpoint_rules(RoleItems,Domain,EtsHash) when is_list(RoleItems) ->
    RoleItems1 = nest_items(RoleItems,Domain),
    fill_endpoint_rules_meta(EtsHash),
    lists:foreach(fun(RoleItem) ->
                        RoleId = maps:get(<<"id">>,RoleItem),
                        PermitEPs = maps:get(<<"permit_endpoints">>,RoleItem),
                        RestrictEPs = maps:get(<<"restrict_endpoints">>,RoleItem),
                        Template = #endpoint_rule{access_mode = 'role',
                                                  role_id = RoleId},
                        fill_endpoint_rules(Template,?Permit,PermitEPs,EtsHash),
                        fill_endpoint_rules(Template,?Restrict,RestrictEPs,EtsHash)
                  end, RoleItems1).

%% ====================================================================
%% Internal functions for authorize
%% ====================================================================

%% @private
%% for endpoint parts ex.[P0,P1,P2,P3,P4] build keys candidates:
%% triangle of [ [<<"**">>],
%%               [P0,<<"**">>],
%%               [P0,P1,<<"**">>],
%%               [P0,P1,P2,<<"**">>],
%%               [P0,P1,P2,P3,<<"**">>],
%%               [P0,P1,P2,P3,<<"*">>], % only in pre last there is one asterisk
%%               [P0,P1,P2,P3,P4] ] % only in last there is no asterisks
candidates(EndpointParts) ->
    case triangle(EndpointParts) of
        [] -> [];
        [L0] -> [lists:reverse(L0),
                 <<"*">>,
                 <<"**">>];
        [L0,L1|Rest] -> [lists:reverse(L0),
                         lists:reverse(L1)++[<<"*">>],
                         lists:reverse(L1)++[<<"**">>]
                        | lists:map(fun(LX) ->
                                        lists:reverse(LX)++[<<"**">>]
                                    end, Rest) ++ [<<"**">>]]
    end.

%% @private
triangle(Parts) ->
    lists:foldl(fun(Part,[]) -> [[Part]];
                   (Part,[Last|_]=Acc) -> [[Part|Last]|Acc]
                end, [], Parts).

%% @private
%% return true if some candidate is matched to endpoints hash
%% check public, authenticated and role access modes
is_matched(Roles,Candidates,Method,EtsHashEndpoints) ->
    lists:any(fun(Key) ->
                    case ets:lookup(EtsHashEndpoints,Key) of
                        [] -> false;
                        [{_,Rules}] ->
                            lists:any(fun(#endpoint_rule{access_mode='public',methods=Methods}) -> check_method(Method,Methods);
                                         (#endpoint_rule{access_mode='authenticated',methods=Methods}) -> check_method(Method,Methods);
                                         (#endpoint_rule{access_mode='role',role_id=RoleId,methods=Methods}) ->
                                             lists:member(RoleId,Roles) andalso check_method(Method,Methods)
                                      end, Rules)
                    end end, Candidates).

%% @private
check_method(_,[<<"*">>]) -> true;
check_method(Method,Methods) -> lists:member(<<"*">>,Methods) orelse lists:member(Method,Methods).

%% ====================================================================
%% Internal functions for build hash
%% ====================================================================

%% @private
nest_items(Items,Domain) when is_list(Items) ->
    EmptyId = ?BU:emptyid(),
    AdapterMap = #{emptyid => EmptyId,
                   fun_name => fun(Item) -> maps:get(<<"code">>,Item) end,
                   fun_id => fun(Item) -> maps:get(<<"id">>,Item) end,
                   fun_parent => fun(Item) -> maps:get(<<"parent_id">>,Item,EmptyId) end,
                   fun_properties => fun(Item) -> maps:get(<<"permit_endpoints">>,Item) end,
                   fun_set_properties => fun(Item,Props) -> Item#{<<"permit_endpoints">> => Props} end,
                   fun_propname => fun(Prop) -> maps:get(<<"url">>,Prop) end,
                   fun_logwarn => fun(S) -> ?LOG("~ts. '~ts' ~ts",[?APP,Domain,S]) end},
    Items2 = ?BU:nest_items(Items,AdapterMap),
    AdapterMap2 = AdapterMap#{fun_properties => fun(Item) -> maps:get(<<"restrict_endpoints">>,Item) end,
                              fun_set_properties => fun(Item,Props) -> Item#{<<"restrict_endpoints">> => Props} end},
    ?BU:nest_items(Items2,AdapterMap2).

%% @private
fill_endpoint_rules_meta(EtsHash) ->
    PrivDir = code:priv_dir(?APP),
    Filepath = filename:join([PrivDir,"general_endpoint_permissions.json"]),
    case filelib:is_regular(Filepath) of
        false -> ok;
        true ->
            {ok,Data} = file:read_file(Filepath),
            try lists:foreach(fun(Item) ->
                                    Endpoints = maps:get(<<"endpoints">>,Item,[]),
                                    case maps:get(<<"access">>,Item) of
                                        <<"public">> ->
                                            Template = #endpoint_rule{access_mode = 'public'},
                                            fill_endpoint_rules(Template,?Permit,Endpoints,EtsHash);
                                        <<"authenticated">> ->
                                            Template = #endpoint_rule{access_mode = 'authenticated'},
                                            fill_endpoint_rules(Template,?Permit,Endpoints,EtsHash);
                                        _ -> ok
                                    end
                              end, jsx:decode(Data,[return_maps]))
            catch E:R:ST ->
                ?LOG('$crash',"Load metadata general enpoints crashed: ~120tp~n\tStack: ~120tp",[{E,R},ST])
            end end.

%% @private
fill_endpoint_rules(Template,KeyPrefix,Endpoints,EtsHash) ->
    lists:foreach(fun(EP) ->
                        Url = maps:get(<<"url">>,EP),
                        Methods = maps:get(<<"methods">>,EP),
                        UrlParts = binary:split(Url,<<"/">>,[global,trim_all]),
                        Rule = Template#endpoint_rule{url = Url,
                                                      methods = Methods},
                        Key = {KeyPrefix,UrlParts},
                        case ets:lookup(EtsHash,Key) of
                            [] -> ets:insert(EtsHash,{Key,[Rule]});
                            [{Key,Rules}] -> ets:insert(EtsHash,{Key,[Rule|Rules]})
                        end
                  end, Endpoints).
