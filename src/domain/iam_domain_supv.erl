%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Domain iam application service supervisor
%%%     Opts:
%%%         domain

-module(iam_domain_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    supervisor:start_link(?MODULE, Opts).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    Opts1 = Opts#{ets_data_users => ets:new(?DU:build_data_name(Domain,?UsersCN), [set,public]),
                  ets_data_roles => ets:new(?DU:build_data_name(Domain,?RolesCN), [set,public]),
                  ets_hash_users => ets:new(?DU:build_hash_name(Domain,users), [set,public]),
                  ets_hash_endpoints => ets:new(?DU:build_hash_name(Domain,endpoints), [set,public]),
                  sync_ref => make_ref()
                 },
    ChildrenSpec = [{?DSRV, {?DSRV, start_link, [Opts1]}, permanent, 1000, worker, [?DSRV]},
                    {?DSearchSrv, {?DSearchSrv, start_link, [Opts1]}, permanent, 1000, worker, [?DSearchSrv]}],
    ?LOG('$info', "~ts. '~ts' supervisor inited", [?APP, Domain]),
    {ok, {{rest_for_one, 10, 2}, ChildrenSpec}}.