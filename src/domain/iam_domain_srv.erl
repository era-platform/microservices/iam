%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.06.2021
%%% @doc Domain iam application service server
%%%     Opts:
%%%         domain
%%%         ets_data_users
%%%         ets_data_roles
%%%         ets_hash_users
%%%         ets_hash_endpoints
%%%         sync_ref

-module(iam_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    %
    subscrid_roles :: binary(),
    subscrid_users :: binary(),
    %
    ref :: reference(),
    timerref :: reference(),
    %
    ets_data_roles :: ets:tab(), % Id :: binary() => RoleItem :: map()
    ets_data_users :: ets:tab(), % Id :: binary() => UserItem :: map()
    %
    ets_hash_endpoints :: ets:tab(), % {Mode :: permit | restrict, Parts :: [Part :: binary()]} => [Rule :: #endpoint_rule{}]
    ets_hash_users :: ets:tab(), % Login ::binary() => UserId :: binary()
    %
    is_paused = false :: boolean(),
    pause_reason :: term(),
    %
    stored_events = [] :: list(),
    %
    search_pid :: pid(),
    search_monref :: reference(),
    %
    modify_pid :: pid(),
    modify_monref :: reference(),
    %
    start_ts :: non_neg_integer(),
    loaded_ts :: non_neg_integer()
}).

-define(TimeoutInit, 10000).
-define(TimeoutRegular, 60000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    ?GLOBAL:gen_server_start_link({gnr,multi,GlobalName}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    SyncRef = ?BU:get_by_key(sync_ref,Opts),
    Self = self(),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    subscrid_roles = SubscrRolesId=?BU:strbin("sid_IAM_roles:~ts;~ts",[node(),Domain]),
                    subscrid_users = SubscrUsersId=?BU:strbin("sid_IAM_users:~ts;~ts",[node(),Domain]),
                    ref = Ref,
                    start_ts = ?BU:timestamp(),
                    ets_data_roles = ?BU:get_by_key(ets_data_roles,Opts),
                    ets_data_users = ?BU:get_by_key(ets_data_users,Opts),
                    ets_hash_endpoints = ?BU:get_by_key(ets_hash_endpoints,Opts),
                    ets_hash_users = ?BU:get_by_key(ets_hash_users,Opts)},
    % ----
    ?BLstore:store_u({?APP,sync,Domain},{'facade',SyncRef,self()}),
    Self ! {'init',Ref},
    % ----
    SubscrRolesOpts = #{<<"ttl_first">> => 20,
                        fun_report => fun(Report) -> Self ! {'subscriber_report',?RolesCN,SyncRef,Report} end},
    SubscrUsersOpts = #{<<"ttl_first">> => 20,
                        fun_report => fun(Report) -> Self ! {'subscriber_report',?UsersCN,SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?RolesCN,self(),SubscrRolesId,SubscrRolesOpts),
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?UsersCN,self(),SubscrUsersId,SubscrUsersOpts),
    % ----
    ?LOG('$info', "~ts. '~ts' srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% Debug restart
handle_call({restart}, _From, State) ->
    {stop,State};

%% --------------
%% Search
handle_call(_, _From, #dstate{is_paused=true,pause_reason=PauseReason}=State) ->
    {reply,PauseReason,State};
%%
handle_call(_, _From, #dstate{search_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    Reply = {retry_after,RetryTimeout,<<"Search-srv process is not ready yet">>},
    {reply,Reply,State};
%%
handle_call({'authenticate',_Args}=Req, From, #dstate{search_pid=Pid}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    gen_server:cast(Pid, {request,Req,FunReply}),
    {noreply,State};
%%
handle_call({'authorize',_Args}=Req, From, #dstate{search_pid=Pid}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    gen_server:cast(Pid, {request,Req,FunReply}),
    {noreply,State};
%%
handle_call({'get_roles',_Args}=Req, From, #dstate{search_pid=Pid}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    gen_server:cast(Pid, {request,Req,FunReply}),
    {noreply,State};

%% --------------
%% Other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% -------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init
handle_info({init,Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',_,SyncRef,Report}, #dstate{sync_ref=SyncRef,timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    State1 = case Report of
                 ok ->
                     Timeout = case ?BU:timestamp() of
                                   NowTS when LoadedTS/=undefined, NowTS - LoadedTS < 5000 -> LoadedTS + 5000 - NowTS;
                                   _ -> 0
                               end,
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#dstate{ref=Ref1,
                                  timerref = erlang:send_after(Timeout, self(), {timer,init,Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'init',Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'regular',Ref}, #dstate{ref=Ref}=State) ->
    Ref1 = make_ref(),
    State1 =  State#dstate{ref=Ref1,
                           timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
    {noreply, State1};

%% --------------
%% On register of search srv. Skip if duplicate.
handle_info({register_srv,?DSearchSrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,search_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DSearchSrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' search-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{search_pid=Pid,
                           search_monref=MonRef}};

%% On register of search srv. Skip if duplicate.
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,modify_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' modify-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{modify_pid=Pid,
                           modify_monref=MonRef}};

%% --------------
%% DOWN search srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{search_monref=MonRef,search_pid=Pid}=State) ->
    {noreply, State#dstate{search_pid=undefined,
                           search_monref=undefined}};

%% --------------
%% DOWN search srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{modify_monref=MonRef,modify_pid=Pid}=State) ->
    {noreply, State#dstate{modify_pid=undefined,
                           modify_monref=undefined}};

%% --------------
%% On notify from DMS about changes in subscriptions
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid_roles=SubscrId,is_paused=true,stored_events=StoredEvents}=State) ->
    State1 = State#dstate{stored_events=[EventInfo|StoredEvents]},
    {noreply, State1};
%%
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid_users=SubscrId,is_paused=true,stored_events=StoredEvents}=State) ->
    State1 = State#dstate{stored_events=[EventInfo|StoredEvents]},
    {noreply, State1};
%%
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid_roles=SubscrId}=State) ->
    State1 = apply_changed_event(EventInfo, State),
    {noreply, State1};
%%
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid_users=SubscrId}=State) ->
    State1 = apply_changed_event(EventInfo, State),
    {noreply, State1};

%% --------------
%% CRUD operation
handle_info({'$call', {FromPid,Ref}, {crud,_,_}=_CrudReq}, #dstate{modify_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    Reply = {retry_later,RetryTimeout,State},
    FromPid ! {'$reply',Ref,Reply},
    {noreply,State};
%% --------------
%% CRUD operation on 'roles', 'users'
handle_info({'$call', {FromPid,Ref}, {crud,CN,{_Operation,_Map}}=CrudReq}, #dstate{domain=Domain,sync_ref=SyncRef,modify_pid=ModifyPid}=State)
  when CN==?RolesCN; CN==?UsersCN ->
    FunReply = fun(Reply) -> FromPid ! {'$reply',Ref,Reply} end,
    Fun = fun() ->
                GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
                Res = (catch ?GLOBAL:gen_server_call(GlobalName, CrudReq, 30000)),
                FunReply(Res)
          end,
    ?DModifySrv:work(ModifyPid,SyncRef,Fun),
    {noreply,State};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------
%% initialization of data. Load all subscriptions from DMS.
%% ------------------------------------------
init_data(#dstate{}=State) ->
    State1 = State#dstate{stored_events=[]},
    Ref1 = make_ref(),
    case catch load(State1) of
        {ok,State2} ->
            State3 = State2#dstate{is_paused = false,
                                   pause_reason = undefined,
                                   loaded_ts = ?BU:timestamp(),
                                   ref=Ref1,
                                   timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
            apply_changed_events(State3);
        {error,_}=Err ->
            State1#dstate{is_paused = true,
                          pause_reason = Err,
                          ref=Ref1,
                          timerref = erlang:send_after(?TimeoutInit, self(), {timer,init,Ref1})}
    end.

%% @private
load(#dstate{domain=Domain,ets_data_roles=EtsRoles,ets_data_users=EtsUsers}=State) ->
    reload_items(Domain,?RolesCN,EtsRoles),
    reload_items(Domain,?UsersCN,EtsUsers),
    build_users_hash(State),
    build_endpoints_hash(State),
    {ok,State}.

%% @private
reload_items(Domain,CN,Ets) ->
    ets:delete_all_objects(Ets),
    case ?DMS_CACHE:read_direct(Domain,CN,#{}) of
        {error,_}=Err -> throw(Err);
        {ok,Items} ->
            lists:foreach(fun(Item) -> ets:insert(Ets,{maps:get(<<"id">>,Item),shrink(CN,Item)}) end, Items)
    end.

%% @private
shrink(_CN,Item) -> Item.

%% @private
build_users_hash(#dstate{ets_data_users=EtsUsers,ets_hash_users=EtsHash}) ->
    ets:delete_all_objects(EtsHash),
    ets:foldl(fun({Id,Item},_) ->
                    Login = maps:get(<<"login">>,Item),
                    ets:insert(EtsHash,{Login,Id})
              end, ok, EtsUsers).

%% @private
build_endpoints_hash(#dstate{domain=Domain,ets_data_roles=EtsRoles,ets_hash_endpoints=EtsHash}) ->
    % fill another ets to avoid EMPTY ets state for authorization process
    Ets = ets:new(tmp,[set]),
    RoleItems = element(2,lists:unzip(ets:tab2list(EtsRoles))),
    ?DEndpoints:build_endpoint_rules(RoleItems,Domain,Ets),
    % softly change items
    KeysOld = ets:foldl(fun({K,_},Acc) -> [K|Acc] end, [], EtsHash),
    KeysNew = ets:foldl(fun({K,_},Acc) -> [K|Acc] end, [], Ets),
    lists:foreach(fun(KeyOld) -> ets:delete(EtsHash,KeyOld) end, KeysOld--KeysNew),
    ets:foldl(fun(Element,_) -> ets:insert(EtsHash,Element) end, ok, Ets),
    ets:delete(Ets),
    ok.

%% ------------------------------------------
%% Applying of events about changes in 'roles' or 'users'
%% ------------------------------------------
apply_changed_events(#dstate{stored_events=StoredEvents}=State) ->
    State1 = lists:foldr(fun(EventInfo,StateX) -> apply_changed_event(EventInfo,StateX) end, State, StoredEvents),
    State1#dstate{stored_events=[]}.

%% Apply one event
apply_changed_event(EventInfo, #dstate{domain=Domain,timerref=TimerRef,loaded_ts=LoadedTS,
                                       ets_data_roles=EtsRoles,ets_data_users=EtsUsers,
                                       ets_hash_users=EtsHashUsers,ets_hash_endpoints=EtsHashEndpoints}=State) ->
    Data = maps:get(<<"data">>,EventInfo),
    CN = maps:get(<<"classname">>,Data),
    Ftimeout = fun(GuaranteeTimeout) ->
                        case ?BU:timestamp() of
                            NowTS when NowTS - LoadedTS > GuaranteeTimeout -> 0;
                            NowTS -> GuaranteeTimeout - (NowTS - LoadedTS)
                        end end,
    case maps:get(<<"operation">>,Data) of
        'clear' when CN==?RolesCN ->
            ets:delete_all_objects(EtsRoles),
            ets:delete_all_objects(EtsHashEndpoints),
            ?DEndpoints:build_endpoint_rules([],Domain,EtsHashEndpoints), % fill from metadata
            State;
        'clear' when CN==?UsersCN ->
            ets:delete_all_objects(EtsUsers),
            ets:delete_all_objects(EtsHashUsers),
            State;
        'reload' ->
            ?BU:cancel_timer(TimerRef),
            Ref1 = make_ref(),
            State#dstate{ref=Ref1,
                         timerref = erlang:send_after(Ftimeout(5000), self(), {timer,init,Ref1})};
        O when CN==?RolesCN ->
            Item = maps:get(<<"entity">>,Data),
            Id = maps:get(<<"id">>,Item),
            FunReload = fun(GuaranteeTimeout) ->
                                ?BU:cancel_timer(TimerRef),
                                Ref1 = make_ref(),
                                State#dstate{ref=Ref1,
                                             timerref = erlang:send_after(Ftimeout(GuaranteeTimeout), self(), {timer,init,Ref1})}
                        end,
            case O of
                'create' ->
                    ItemShrink = shrink(CN,Item),
                    ets:insert(EtsRoles,{Id,ItemShrink}),
                    FunReload(5000);
                'update' ->
                    ItemShrink = shrink(CN,Item),
                    ets:insert(EtsRoles,{Id,ItemShrink}),
                    FunReload(5000);
                'delete' ->
                    ets:delete(EtsRoles,Id),
                    FunReload(5000);
                'corrupt' ->
                    FunReload(5000)
            end;
        O when CN==?UsersCN ->
            Item = maps:get(<<"entity">>,Data),
            Id = maps:get(<<"id">>,Item),
            case O of
                'create' ->
                    ItemShrink = shrink(CN,Item),
                    ets:insert(EtsUsers,{Id,ItemShrink}),
                    Login = maps:get(<<"login">>,Item),
                    ets:insert(EtsUsers,{Login,Id}),
                    State;
                'update' ->
                    LoginPrev = case ets:lookup(EtsUsers,Id) of
                                    [] -> undefined;
                                    [{_,ItemOld}] -> maps:get(<<"login">>,ItemOld)
                                end,
                    ItemShrink = shrink(CN,Item),
                    ets:insert(EtsUsers,{Id,ItemShrink}),
                    case maps:get(<<"login">>,Item) of
                        LoginPrev -> ok;
                        Login ->
                            ets:delete(EtsHashUsers,LoginPrev),
                            ets:insert(EtsHashUsers,{Login,Id})
                    end,
                    State;
                'delete' ->
                    ets:delete(EtsUsers,Id),
                    Login = maps:get(<<"login">>,Item),
                    ets:delete(EtsHashUsers,Login),
                    State;
                'corrupt' ->
                    spawn(fun() -> on_corrupt(?UsersCN,Domain,Item,{EtsUsers,EtsHashUsers}) end),
                    State
            end end.

%% @private
%% Async process
on_corrupt(?UsersCN,Domain,Item,{EtsUsers,EtsHashUsers}) ->
    Id = maps:get(<<"id">>,Item),
    LoginPrev = maps:get(<<"login">>,Item),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Args = {'read',#{class => ?UsersCN,
                     object => {'entity',Id,[]},
                     qs => #{}}},
    case catch ?GLOBAL:gen_server_call(GlobalName,{crud,?UsersCN,Args}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Call to DMS (read user '~ts', id='~ts', login='~ts') crashed: ~n\t~120tp",[Domain,Id,LoginPrev,Reason]),
            ok;
        {error,_}=Err ->
            ?LOG('$error',"Call to DMS (read users '~ts', id='~ts', login='~ts') error: ~n\t~120tp",[Domain,Id,LoginPrev,Err]),
            ets:delete(EtsUsers,Id),
            ets:delete(EtsHashUsers,LoginPrev);
        {ok,Item} ->
            ItemShrink = shrink(?UsersCN,Item),
            ets:insert(EtsUsers,{Id,ItemShrink}),
            case maps:get(<<"login">>,Item) of
                LoginPrev -> ok;
                Login ->
                    ets:delete(EtsHashUsers,LoginPrev),
                    ets:insert(EtsHashUsers,{Login,Id})
            end end.

%% -----------------------------------------
%% @private
retry_timeout(#dstate{start_ts=StartTS}) ->
    Diff = ?BU:timestamp() - StartTS,
    erlang:min(5000,erlang:max(20,Diff)).